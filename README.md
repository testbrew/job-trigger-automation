### Job Trigger ("play") automation
![Pipeline]

Project main goal it to automate "play" on a specific job.   

It is useful for us to schedule a deployment to staging. This is to replace the daily manual operation.   
The projects uses GitLab API.

#### Precondition
Install the following application
```
httpie
jq
```

The following variables need to be present
```
PROJECT_ID=2;
JOB_NAME=deploy-staging;
JOB_SCOPE=manual;
PRIVATE_TOKEN=$(cat ~/.gitlab-token)
```

###### Note: Don't want to run on local? check the docker run

#### Run locally
To run the project locally check the `.trigger-job-local.sh`   

#### Docker run
```
docker build -t <image_name> .
docker run -it -v ${PWD}:/usr/src/project --workdir /usr/src/project --entrypoint /bin/sh
... set variables
./trigger-job.sh
```



[pipeline]: https://gitlab.com/testbrew/job-trigger-automation/badges/master/pipeline.svg
