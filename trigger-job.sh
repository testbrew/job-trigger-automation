#! /bin/bash

if [ -z ${PRIVATE_TOKEN+x} ]; then
    PRIVATE_TOKEN=$(cat ~/.gitlab-token);
fi

############### Check Variables ###############################################
if [ -z ${PROJECT_ID+x} ]; then echo "PROJECT_ID is unset"; exit 1; fi
if [ -z ${JOB_NAME+x} ]; then echo "JOB_NAME is unset"; exit 1; fi
if [ -z ${JOB_SCOPE+x} ]; then echo "JOB_SCOPE is unset"; exit 1; fi
if [ -z ${PRIVATE_TOKEN+x} ]; then echo "PRIVATE_TOKEN is unset"; exit 1; fi
###############################################################################

echo Getting latest successful master pipeline from: https://gitlab.com/projects/$PROJECT_ID
PIPELINE_ID=$(http GET https://gitlab.com/api/v4/projects/$PROJECT_ID/pipelines status==success ref==master order_by==id sort==desc "PRIVATE-TOKEN:$PRIVATE_TOKEN" | jq '.[0].id');
JOB_ID=$(http GET https://gitlab.com/api/v4/projects/$PROJECT_ID/pipelines/$PIPELINE_ID/jobs scope==$JOB_SCOPE "PRIVATE-TOKEN:$PRIVATE_TOKEN" | jq -c -r --arg JOB_NAME "$JOB_NAME" '.[] | select(.name==$JOB_NAME) | .id');

if [ -z "$JOB_ID" ]; then
   echo Job \'$JOB_NAME\' on pipeline \'$PIPELINE_ID\' is not in scope \'$JOB_SCOPE\'
else
   http POST https://gitlab.com/api/v4/projects/$PROJECT_ID/jobs/$JOB_ID/play "PRIVATE-TOKEN:$PRIVATE_TOKEN" | jq . ;
   echo Job \'$JOB_NAME\' on pipeline \'$PIPELINE_ID\' with scope \'$JOB_SCOPE\' has been triggered
fi
